import RestClient from '../client/rest.client';
import URI from '../config/uri';

class AuthorService {

    createAuthor(author) {
        return RestClient.doPost(URI.POST_AUTHOR, author);
    }

    searchAuthorByNameOrSurname(NameOrSurname) {
        return RestClient.doGet(URI.SEARCH_AUTHORS_BY_NAME_OR_SURNAME + NameOrSurname);
    }

    deleteAuthor(authorId) {
        return RestClient.doDelete(URI.DELETE_AUTHOR_BY_ID + authorId);
    }

    updateAuthor(authorId, author) {
        return RestClient.doPut(URI.PUT_AUTHOR + authorId, author);
    }
}

export default new AuthorService();