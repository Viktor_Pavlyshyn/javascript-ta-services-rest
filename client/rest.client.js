import ConstantDate from '../config/constantdata';
const superTest = require('supertest');
const request = superTest(ConstantDate.endpointURL)

class RestClient {

    async doPost (uri, body){
        return await request.post(uri)
        .set(ConstantDate.contentType)
        .send(body);
    };

    async doGet (uri){
        return await request.get(uri)
        .set(ConstantDate.contentType);
    };

    async doPut (uri, body) {
        return await request.put(uri)
        .set(ConstantDate.contentType)
        .send(body);
    };

    async doDelete (uri) {
        return await request.delete(uri)
        .set(ConstantDate.contentType);
    } 
}

export default new RestClient();