import {expect} from 'chai';
import LibraryData from '../config/library.data';
import AuthorService from '../service/author.service';


describe('Create', () => {

    const author = LibraryData.author;

    describe('Creat Author', () => {

        it('POST Author', () => {
            AuthorService.createAuthor(author)
            .then(response => {
                expect(201).to.be.eq(response.status);
                expect(author).to.be.eq(response.body);
            });
        });

        it('DELETE Author', () => {
            AuthorService.deleteAuthor(author.authorId)
            .then(response => {
                expect(204).to.be.eq(response.status);
            });
        });
    });

    describe('Create exist Author', () => {

        it('POST Author', () => {
            AuthorService.createAuthor(author)
            .then(response => {
                expect(201).to.be.eq(response.status);
            });
        });

        it('POST exist Author', () => {
            AuthorService.createAuthor(author)
            .then(response => {
                expect(409).to.be.eq(response.status)
                expect('Conflict').to.be.eq(response.error)
                expect('Incorrect response object.').to.be.eq(response.text)
            });
        });

        it('DELETE Author', () => {
         AuthorService.deleteAuthor(author.authorId)
            .then(response => {
                expect(204).to.be.eq(response.status);
            });
        });
    });
});