import {expect} from 'chai';
import LibraryData from '../config/library.data';
import AuthorService from '../service/author.service';

describe('Delete', () => {

    const author = LibraryData.author;

    describe('DELETE Author', () => {

        it('POST Author', () => {
            AuthorService.createAuthor(author)
            .then(response => {
                expect(201).to.be.eq(response.status);
            });
        });

        it('DELETE Author', () => {
            AuthorService.deleteAuthor(author.authorId)
            .then(response => {
                expect(204).to.be.eq(response.status);
            });
        });
    });

    describe('DELETE not exist Author', () => {
        
        it('POST Author', () => {
            AuthorService.createAuthor(author)
            .then(response => {
                expect(201).to.be.eq(response.status);
            });
        });

        it('DELETE Author', () => {
            AuthorService.deleteAuthor(author.authorId)
            .then(response => {
                expect(204).to.be.eq(response.status);
            });
        });

        it('DELETE not exist Author', () => {
            AuthorService.deleteAuthor(author.authorId)
            .then(response => {
                expect(404).to.be.eq(response.status);
                expect(`Author with 'authorId' = '${author.authorId}' doesn't exist!`).to.be.eq(response.text);
            });
        });
    });
});