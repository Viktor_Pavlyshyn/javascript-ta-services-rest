import {expect} from 'chai';
import LibraryData from '../config/library.data';
import AuthorService from '../service/author.service';

describe('Get by name or surname', () => {

    const author = LibraryData.author;

    describe('GET Author', () => {

        it('POST Author', () => {
            AuthorService.createAuthor(author)
            .then(response => {
                expect(201).to.be.eq(response.status);
            });
        });

        it('GET Author', () => {
            AuthorService.searchAuthorByNameOrSurname(author.authorId)
            .then(response => {
                expect(200).to.be.eq(response.status);
                expect(author).to.be.eq(response.body);
            });
        });

        it('DELETE Author', () => {
            AuthorService.deleteAuthor(author.authorId)
            .then(response => {
                expect(204).to.be.eq(response.status);
            });
        });
    });

    describe('GET not exist Author', () => {

        it('POST Author', () => {
            AuthorService.createAuthor(author)
            .then(response => {
                expect(201).to.be.eq(response.status);
            });
        });

        it('DELETE Author', () => {
            AuthorService.deleteAuthor(author.authorId)
            .then(response => {
                expect(204).to.be.eq(response.status);
            });
        });

        it('GET not exist Author', () => {
            AuthorService.searchAuthorByNameOrSurname(author.authorId)
            .then(response => {
                expect(404).to.be.eq(response.status);
                expect(`Author with 'authorId' = '${author.authorId}' doesn't exist!`).to.be.eq(response.text);
            });
        });
    });
})