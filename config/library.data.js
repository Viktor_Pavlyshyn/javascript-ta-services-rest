const faker = require('faker');

export default {
    
    author: {
     authorId: faker.random.number(),
     authorName: {
            first: faker.name.firstName(),
            second: faker.name.lastName()
        },
        nationality: faker.lorem.word(),
        birth: {
            date: '1111-11-11',
            country: faker.name.jobArea(),
            city: faker.name.suffix()
        },
        authorDescription: faker.lorem.paragraphs()
    },

    book: {
        id: faker.random.number(),
        name: faker.name.firstName(),
        language: faker.lorem.word(),
        description: faker.lorem.paragraphs(),
        additional: {
            pageCount: faker.random.number(),
            size: {
                height: faker.random.float(),
                width: faker.random.float(),
                length: faker.random.float()
            }
        }  
    },

    genre: {
        id: faker.random.number(),
        name: faker.name.firstName(),
        description: faker.lorem.paragraphs()
    }
}