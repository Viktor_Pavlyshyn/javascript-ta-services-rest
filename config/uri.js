export default{
    
    POST_AUTHOR: '/api/library/author',
    DELETE_AUTHOR_BY_ID: '/api/library/author/',
    SEARCH_AUTHORS_BY_NAME_OR_SURNAME: '/api/library/authors/search?query=',
    PUT_AUTHOR: '/api/library/author/'

}